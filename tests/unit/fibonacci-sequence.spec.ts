import { FibonacciSequence } from "@/lib/fibonacci-sequence";

describe('fibonacci', () => {
  it('default y and z', () => {
    const fibonacci = new FibonacciSequence();
    const result = fibonacci.getSequence(6, 1, 2);
    expect(result).toEqual([1, 1, 2, 3, 5, 8]);
  });

  it('custom y and z', () => {
    const fibonacci = new FibonacciSequence();
    const result = fibonacci.getSequence(6, 2, 3);
    expect(result).toEqual([1, 1, 1, 2, 2, 3]);
  });
})
