export class FibonacciSequence {
	public getSequence(number: number, y: number, z: number): number[] {
		const sequence: number[] = [];
		y = y > 0 ? y : 1;
		z = z > 0 ? z : 2;

		for (let i = 0; i < number; i++) {
			if (!sequence[y] && !sequence[z]) { 
				sequence[i] = 1; 
			} else {
				sequence[i] = sequence[i - y] + sequence[i - z];
			}
		}

		return sequence;
	}
}